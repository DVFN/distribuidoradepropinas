package com.colegio.devin.distribuidoradepropinas;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CalcularDistribucion extends AppCompatActivity {
    //Edit Text Total Propina
    @Bind(R.id.etxtToPro)
    EditText etxtToPro;
    //Edit Text numero de Trabajadores
    @Bind(R.id.etxtNTrabajadores)
    EditText etxtNTrabajadores;
    //TextView Total de a Distribucion
    @Bind(R.id.txtTotalDis)
    TextView txtTotalDis;
    @Bind(R.id.btnMasNT)
    Button btnMasNT;
    @Bind(R.id.btnMenosNT)
    Button btnMenosNT;
    @Bind(R.id.btnCalcularDis)
    Button btnCalcularDis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcular_distribucion);
        ButterKnife.bind(this);
        if (getIntent().getStringExtra("txtTotal").equals("Total de propina")){

        }else{
            String sinQ= getIntent().getStringExtra("txtTotal").substring(0,1);
            String conTpro= getIntent().getStringExtra("txtTotal").substring(2);
            etxtToPro.setText(conTpro);
        }
    }

    @OnClick(R.id.btnMasNT)
    public void handleClickMasNT(){
        Integer mas = (Integer.parseInt(etxtNTrabajadores.getText().toString()) + 1);
        etxtNTrabajadores.setText(Integer.toString(mas));
    }

    @OnClick(R.id.btnMenosNT)
    public void handleClickMenosNT(View view){
        Integer menos = (Integer.parseInt(etxtNTrabajadores.getText().toString())- 1);
        if (menos >= 0){
            etxtNTrabajadores.setText(Integer.toString(menos));
        }else{
            Snackbar.make(view,"No se puede disminuir de 0", Snackbar.LENGTH_SHORT).show();
        }
    }
    @OnClick(R.id.btnCalcularDis)
    public void handleClickCalcularDis(View view){
        try{

            Double nPro =Double.parseDouble(etxtToPro.getText().toString());
            Double nTra = Double.parseDouble(etxtNTrabajadores.getText().toString());
            Double totalDis = nPro /nTra;
            if(totalDis == nPro/0){
                Snackbar.make(view, "No puede no haber trabajadores",Snackbar.LENGTH_SHORT).show();
            }else{
                txtTotalDis.setText("Q."+Double.toString(totalDis));
            }
        }catch (Exception e){
            Snackbar.make(view, "No se ha ingresado un numero de trabajadores",Snackbar.LENGTH_SHORT).show();
        }
    }

}
