package com.colegio.devin.distribuidoradepropinas;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListaPropina extends AppCompatActivity  {

    @Bind(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.recicler_layout);
        ButterKnife.bind(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(ListaPropina.this));
    }

    @Override
    public void onResume(){
        super.onResume();
        recyclerView.setAdapter(new AdapterPropinas(getIntent().getStringArrayListExtra("arrayTotales"), ListaPropina.this));
    }
}
