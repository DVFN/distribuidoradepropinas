package com.colegio.devin.distribuidoradepropinas;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by devin on 17/07/2016.
 */
public class AdapterPropinas extends RecyclerView.Adapter<AdapterPropinas.PropinaHolder>  {

    List<String> lista;
    Context context;

    public AdapterPropinas(List<String> lista, Context context) {
        this.lista = lista;
        this.context = context;
    }

    @Override
    public PropinaHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View vista= LayoutInflater.from(parent.getContext()).inflate(R.layout.propina_item, parent, false);
        return new PropinaHolder(vista);
    }

    @Override
    public void onBindViewHolder(PropinaHolder holder, int position){
        holder.textView.setText(lista.get(position));
    }

    @Override
    public int getItemCount(){
        return lista.size();
    }

    public static class PropinaHolder extends RecyclerView.ViewHolder{
        @Bind(R.id.textView)
        TextView textView;

        public PropinaHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
