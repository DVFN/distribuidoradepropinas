package com.colegio.devin.distribuidoradepropinas;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


import java.util.ArrayList;


import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CalculadoraDePropinas extends AppCompatActivity {
    @Bind(R.id.btnCalcular)
    Button btnCalcular;
    @Bind(R.id.etxtToGa)
    EditText etxtToGa  ;
    @Bind(R.id.etxtToPro)
    EditText etxtPor ;
    @Bind(R.id.btnMas)
    Button btnMas ;
    @Bind(R.id.btnTipTotales)
    Button btnTipTotales;
    @Bind(R.id.btnMenos)
    Button btnMenos;
    @Bind(R.id.txtTotal)
    TextView txtTotal;

    ArrayList <String> arrayTotales = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora_de_propinas);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnMas)
    public void handleClickMas(){
        Double mas = (Double.parseDouble(etxtPor.getText().toString()) + 1);
        etxtPor.setText(Double.toString(mas));
    }

    @OnClick(R.id.btnMenos)
    public void handleClickMenos(View view){
        Double menos = (Double.parseDouble(etxtPor.getText().toString())- 1);
        if (menos >= 0){
            etxtPor.setText(Double.toString(menos));
        }else{
            Snackbar.make(view,"No se puede disminuir de 0", Snackbar.LENGTH_SHORT).show();
        }

    }
    @OnClick(R.id.btnCalcular)
    public void handleClickCalcular(View view){
        try {
            Double calcular = (Double.parseDouble(etxtToGa.getText().toString()));
            Double porcentaje = (Double.parseDouble(etxtPor.getText().toString()));
            Double total = calcular * porcentaje /100 ;
            txtTotal.setText("Q." + Double.toString(total));
        }catch (Exception e){
            Snackbar.make(view,"No hay datos ingresados",Snackbar.LENGTH_SHORT).show();
        }
    }
    @OnClick(R.id.btnTipTotales)
    public void handleClickTipTotales(){
        arrayTotales.add(txtTotal.getText().toString());
        Intent intent= new Intent(CalculadoraDePropinas.this, ListaPropina.class);
        intent.putExtra("arrayTotales", arrayTotales);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent = new Intent(CalculadoraDePropinas.this, CalcularDistribucion.class);
        intent.putExtra("txtTotal", txtTotal.getText().toString());
        startActivity(intent);


        return super.onOptionsItemSelected(item);
    }
}
